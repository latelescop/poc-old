#!/bin/bash

#export OTB_LOGGER_LEVEL=DEBUG
export OTB_MAX_RAM_HINT=2048

PIXSIZE="10m"
CRS="EPSG:32631"
EXTENT="551980.0 4812020.0 587980.0 4848020.0"
NDVI_EXP='ndvi(im1b1,im1b4)'
DVI_EXP='im1b4 - im1b1'
EVI_EXP='2.5 * ((im1b4 / 10000) - (im1b1 / 10000)) / ((im1b4 / 10000 + 6.0 * im1b1 / 10000 - 7.5 * im1b3 / 10000) + 1.0)'
DIFF_EXP='(im1b1 <= 0) && (im3b1 <= 0) ? im4b1 - im2b1 : -99999'

cd /media/nas/projects/POC_OLD/traitements/s2
#rm -rf /tmp/otb roi ndvi clouds_raster clouds_vector
mkdir -p /tmp/otb roi ndvi dvi clouds_raster clouds_vector diff stats

if [ -d "data" ]; then
    for d in `ls data`; do
        date=${d:11:8}
        r=("data/$d/"*"_B4.tif")
        g=("data/$d/"*"_B3.tif")
        b=("data/$d/"*"_B2.tif")
        ir=("data/$d/"*"_B8.tif")
        clouds=("data/$d/MASKS/"*"_CLM_R1.tif")
        outimg=roi/$date.tif

        if [ ! -e $outimg ]; then
            # ROI
            otbcli_ExtractROI -startx 5200 -starty 5200 -sizex 3600 -sizey 3600 -in $r -out /tmp/otb/r.tif int16
            otbcli_ExtractROI -startx 5200 -starty 5200 -sizex 3600 -sizey 3600 -in $g -out /tmp/otb/g.tif int16
            otbcli_ExtractROI -startx 5200 -starty 5200 -sizex 3600 -sizey 3600 -in $b -out /tmp/otb/b.tif int16
            otbcli_ExtractROI -startx 5200 -starty 5200 -sizex 3600 -sizey 3600 -in $ir -out /tmp/otb/ir.tif int16
            otbcli_ExtractROI -startx 5200 -starty 5200 -sizex 3600 -sizey 3600 -in $clouds -out clouds_raster/$date.tif uint8
            # Stack
            otbcli_ConcatenateImages -il /tmp/otb/r.tif /tmp/otb/g.tif /tmp/otb/b.tif /tmp/otb/ir.tif -out $outimg int16
            # Clouds
            gdal_polygonize.py clouds_raster/$date.tif /tmp/otb/clouds.shp
            ogr2ogr -sql "SELECT * from clouds where \"DN\" > 0" clouds_vector/$date.shp /tmp/otb/clouds.shp
            # Cleaning
            rm -f /tmp/otb/*
        fi
    done
fi

# Compute NDVI and DVI
rm -f dates.txt
for d in `ls roi`; do
    echo -n $d | sed -e 's/\.tif/ /' >> dates.txt
    outimg=ndvi/"$d"
    if [ ! -e $outimg ]; then
        otbcli_BandMathX -il roi/$d -exp "$NDVI_EXP" -out $outimg float
    fi

    outimg=dvi/"$d"
    if [ ! -e $outimg ]; then
        otbcli_BandMathX -il roi/$d -exp "$DVI_EXP" -out $outimg int16
    fi
done

#rm -f stats/parcelle_*stats.*
tiles=`ls ndvi/*.tif`
if [ ! -e ndvi_stack.vrt ]; then
    gdalbuildvrt -separate ndvi_stack.vrt $tiles -a_srs $CRS -tr 10 10
fi

if [ ! -e stats/parcelles_ndvi_stats.shp ]; then
    otbcli_ZonalStatistics -in ndvi_stack.vrt -inzone vector -inzone.vector.in ../parcelles_utm.shp -out vector -out.vector.filename stats/parcelles_ndvi_stats.shp &
fi

tiles=`ls clouds_raster/*.tif`
if [ ! -e clouds_stack.vrt ]; then
    gdalbuildvrt -separate clouds_stack.vrt $tiles -a_srs $CRS -tr 10 10
fi

last_img=
for img in `ls ndvi`; do
    outimg=diff/"$img"
    if [ ! -e $outimg ]; then
        if [ -z $last_img ]; then
            otbcli_BandMathX -il ndvi/$d -exp "-99999" -out /tmp/otb/diff.tif float
        else
            otbcli_BandMathX -il clouds_raster/$last_img ndvi/$last_img clouds_raster/$img ndvi/$img -exp "$DIFF_EXP" -out /tmp/otb/diff.tif float
        fi
        otbcli_ManageNoData -in /tmp/otb/diff.tif -out $outimg -mode changevalue -mode.changevalue.newv -99999
    fi
    rm -rf /tmp/otb/diff/*
    last_img=$img
done

tiles=`ls diff/*.tif`
if [ ! -e diff_stack.vrt ]; then
   gdalbuildvrt -separate diff_stack.vrt $tiles -a_srs $CRS -srcnodata -99999
fi

if [ ! -e stats/parcelles_diff_stats.shp ]; then
   otbcli_ZonalStatistics -in diff_stack.vrt -inzone vector -inzone.vector.in ../parcelles_utm.shp -out vector -out.vector.filename stats/parcelles_diff_stats.shp &
fi

# Gap filled not working "Pixel and mask have different sizes"
# if [ ! -e clouds_superimpose.tif ]; then
#     otbcli_Superimpose -inr ndvi_stack.vrt -inm clouds_stack.vrt -out clouds_superimpose.tif uint8
# fi

# if [ ! -e ndvi_stack_filled.tif ]; then
#     otbcli_ImageTimeSeriesGapFilling -in ndvi_stack.vrt -mask clouds_superimpose.tif -comp 4 -id dates.txt -it spline -out ndvi_stack_filled.tif
# fi

# if [ ! -e stats/parcelles_ndvi_filled_stats.shp ]; then
#     otbcli_ZonalStatistics -in ndvi_stack_filled.tif -inzone vector -inzone.vector.in ../parcelles_utm.shp -out vector -out.vector.filename stats/parcelles_ndvi_filled_stats.shp
# fi

# if [ ! -e stats/parcelles_tup_ndvi_stats.shp ]; then
#     ogr2ogr -s_srs EPSG:2154 -t_srs $CRS /tmp/tup.shp ../other_data/inter_old_tup/inter_old_tup.shp
#     otbcli_ZonalStatistics -in ndvi_stack_filled.tif -inzone vector -inzone.vector.in /tmp/tup.shp -out vector -out.vector.filename stats/parcelles_tup_ndvi_stats.shp
# fi

# DVI
# tiles=`ls dvi/*.tif`
# if [ ! -e dvi_stack.vrt ]; then
#    gdalbuildvrt -separate dvi_stack.vrt $tiles -a_srs $CRS
# fi

# if [ ! -e stats/parcelles_dvi_stats.shp ]; then
#    otbcli_ZonalStatistics -in dvi_stack.vrt -inzone vector -inzone.vector.in ../parcelles_utm.shp -out vector -out.vector.filename stats/parcelles_dvi_stats.shp
# fi
