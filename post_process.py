import os
import datetime

import numpy as np
import pandas as pd
import geopandas as gpd
import sklearn
from sklearn.cluster import KMeans, DBSCAN

import matplotlib.pyplot as plt
#plt.style.use('dark_background')

from func_utils import *

CRS="EPSG:32631"
EXTENT="551980.0 4812020.0 587980.0 4848020.0"
OVERWRITE=False

os.chdir('/media/nas/projects/POC_OLD/traitements')

# Dates
DATES = open('s2/dates.txt', 'r').read().split()
DTIMES = to_datetimes(DATES)
print(f"Série temporelle Sentinel 2 constituée de {len(DTIMES)} images")

DATES_PLEIADES = open('pleiades/dates.txt', 'r').read().split()
DTIMES_PLEIADES = to_datetimes(DATES_PLEIADES)
print(f"Série temporelle Pléiades constituée de {len(DTIMES_PLEIADES)} images")

#fig, ax = plt.subplots(figsize=(12,2))
#plt.scatter(DTIMES, np.zeros_like(DTIMES), marker='.')
#fig.savefig('../plots/date_plot.png', bbox_inches='tight')

# Parcelles
parcelles = parcelles_pleiades = gpd.read_file('parcelles.shp')

cluster = sklearn.cluster.KMeans(n_clusters=6, n_init=1, random_state=5)
res=cluster.fit(parcelles[['bati_pc','arbo_pct']])
km_labels = res.labels_
parcelles = parcelles.join(pd.Series(km_labels).rename('classif'))

parcelles.set_index('gid', drop=False, inplace=True, verify_integrity=True)
parcelles['aire'] = parcelles.area

parcelles_mtp = parcelles[parcelles['commune'] == '34172']

parcelles_stats = gpd.read_file('s2/stats/parcelles_ndvi_stats.shp')
parcelles_stats.set_index('gid', drop=False, inplace=True, verify_integrity=True)
parcelles_stats=parcelles_stats.join(parcelles['classif'])

parcelles_stats['aire'] = parcelles_stats.area
parcelles_orig = parcelles_stats.filter(regex='[a-z]$')

parcelles_OLD = parcelles_orig[(parcelles_orig.zone_old != 'aucune') & (parcelles_orig.zone_old != 'sans obligation')]
parcelles_OLD_full = parcelles[(parcelles.zone_old != 'aucune') & (parcelles.zone_old != 'sans obligation')]
parcelles_OLD_mtp = parcelles_OLD_full[parcelles_OLD_full['commune'] == '34172']

#parcelles[(parcelles.zone_old != 'aucune') & (parcelles.zone_old != 'sans obligation')].to_excel('/home/vidlb/Téléchargements/export_parcelles_OLD.xls')

parcelles_OLD[['aire','zone_old']].groupby('zone_old').median()
parcelles_OLD[['aire','zone_old']].groupby('zone_old').median().to_csv('s2/stats/median_area_OLD.csv')

parcelles_OLD[['aire','zone_old']].groupby('zone_old').mean()
parcelles_OLD[['aire','zone_old']].groupby('zone_old').mean().to_csv('s2/stats/mean_area_OLD.csv')

parcelles_centro = parcelles.copy()
parcelles_centro.geometry = parcelles_centro.geometry.centroid

old_idx = parcelles_OLD_full.index
other_idx = parcelles_orig.index[np.invert(parcelles_orig.index.isin(old_idx))]
print(f"Parcelles OLD : {len(old_idx)}\nParcelles autres : {len(other_idx)}")

old_mtp_idx = parcelles_OLD_mtp.index
other_mtp_idx = parcelles_mtp.index[np.invert(parcelles_mtp.index.isin(old_mtp_idx))]
print(f"Montpellier :\n    Parcelles OLD : {len(old_mtp_idx)}\n    Parcelles autres : {len(other_mtp_idx)}")

# Contrôles
if not os.path.exists('controle_df.csv') or OVERWRITE:
    controle = pd.read_csv('fiche_controle.csv')
    controle_df = controle.copy()[controle.columns[:1]]

    controle_df['id'] = '34172000'+ controle_df['Parcelle'].str[:6]
    controle_df['c1_date'] = pd.to_datetime(controle[controle.columns[1]], dayfirst=True)
    controle_df['c1_conformite'] = controle[controle.columns[2]] == 1
    controle_df['c2_date'] = pd.to_datetime(controle[controle.columns[3]], dayfirst=True)
    controle_df['c2_conformite'] = controle[controle.columns[4]] == 1

    controle_df.to_csv('controle_df.csv')

    parcelles_OLD_mtp = parcelles_mtp[(parcelles_mtp.zone_old != 'aucune') & (parcelles_mtp.zone_old != 'sans obligation')]
    controle_mtp = parcelles_OLD_mtp.merge(controle_df, left_on='id', right_on='id')

    if not os.path.exists("controle_mtp.gpkg") or OVERWRITE:
        controle_mtp.to_file("controle_mtp.gpkg", driver="GPKG")
else:
    controle_mtp = gpd.read_file('controle_mtp.gpkg')
    #controle_mtp.set_index('gid', drop=False, inplace=True, verify_integrity=True)
    # => error : duplicates
    controle_mtp.c1_date = pd.to_datetime(controle_mtp.c1_date)
    controle_mtp.c2_date = pd.to_datetime(controle_mtp.c2_date)

# controle_mtp_orig = controle_mtp.copy()

# ctrl_mtp_nodates = controle_mtp[(controle_mtp.c1_date.isnull()) & (controle_mtp.c2_date.isnull())].copy()
# controle_mtp = controle_mtp[np.invert(controle_mtp.index.isin(ctrl_mtp_nodates.index))]

# controle_mtp_2019 = controle_mtp[((controle_mtp.c1_date > '2019-01-01') | (controle_mtp.c2_date > '2019-01-01'))]

# controle_true = controle_mtp[((controle_mtp.c1_conformite == True) | (controle_mtp.c2_conformite == True))]
# controle_false = controle_mtp[np.invert(controle_mtp.index.isin(controle_true.index))]
# controle_true_2019 = controle_mtp[(controle_mtp.index.isin(controle_true.index)) & (controle_mtp.index.isin(controle_mtp_2019.index))]
# controle_false_2019 = controle_mtp[(controle_mtp.index.isin(controle_false.index)) & (controle_mtp.index.isin(controle_mtp_2019.index))]

# NDVI mean
time_serie_mean = pd.DataFrame(parcelles_stats.filter(regex='mean_*', axis=1), index=parcelles_stats.index)
time_serie_mean.columns = pd.DatetimeIndex(DTIMES)

if not os.path.exists("s2/stats/ndvi_mean.csv") or OVERWRITE:
    time_serie_mean.to_csv("s2/stats/ndvi_mean_raw.csv")
    ndvi_mean = clean_serie(time_serie_mean, parcelles_orig)
    ndvi_mean.to_csv("s2/stats/ndvi_mean.csv")
elif os.path.exists("s2/stats/ndvi_mean.csv"):
    ndvi_mean = pd.read_csv("s2/stats/ndvi_mean.csv", index_col=0, names=pd.DatetimeIndex(DTIMES), header=0)

if not os.path.exists('s2/stats/ndvi_mean_diff.csv'):
    ndvi_mean_diff = ndvi_mean.apply(reject_outliers, args=(3,), axis=1).interpolate('time', axis=1).diff(axis=1)
    ndvi_mean_diff.to_csv("s2/stats/ndvi_mean_diff.csv")
else:
    ndvi_mean_diff = pd.read_csv("s2/stats/ndvi_mean_diff.csv", index_col=0, names=pd.DatetimeIndex(DTIMES), header=0)

# NDVI std
time_serie_std = pd.DataFrame(parcelles_stats.filter(regex='stdev_*', axis=1), index=parcelles_stats.index)
time_serie_std.columns = pd.DatetimeIndex(DTIMES)

if not os.path.exists("s2/stats/ndvi_std.csv") or OVERWRITE:
    time_serie_std.to_csv("s2/stats/ndvi_std_raw.csv")
    ndvi_std = pd.DataFrame(np.where(ndvi_mean.isnull(), np.nan, time_serie_std))
    ndvi_std.columns = pd.DatetimeIndex(DTIMES)
    ndvi_std.to_csv("s2/stats/ndvi_std.csv")
elif os.path.exists("s2/stats/ndvi_std.csv"):
    ndvi_std = pd.read_csv("s2/stats/ndvi_std.csv", index_col=0, names=pd.DatetimeIndex(DTIMES), header=0)

# NDVI diff
ndvi_diff = gpd.read_file('s2/stats/parcelles_diff_stats.shp')
ndvi_diff.set_index('gid', drop=False, inplace=True, verify_integrity=True)
ndvi_diff_mean = pd.DataFrame(ndvi_diff.filter(regex='mean_*', axis=1), index=parcelles_stats.index)

ndvi_diff_mean = pd.DataFrame(np.where(ndvi_diff_mean < -10, np.nan, ndvi_diff_mean), index=parcelles_stats.index)
ndvi_diff_mean.columns = pd.DatetimeIndex(DTIMES)

ndvi_diff_std = pd.DataFrame(ndvi_diff.filter(regex='stdev_*', axis=1), index=parcelles_stats.index)

ndvi_diff_std = pd.DataFrame(np.where(ndvi_diff_mean.isnull(), np.nan, ndvi_diff_std), index=parcelles_stats.index)
ndvi_diff_std.columns = pd.DatetimeIndex(DTIMES)

# Filled
# parcelles_stats_filled = gpd.read_file("s2/stats/parcelles_ndvi_filled_stats.shp")
# parcelles_stats_filled.set_index('gid', drop=False, inplace=True, verify_integrity=True)
# parcelles_stats_filled = parcelles_stats_filled.join(parcelles['classif'])
# parcelles_stats_filled['aire'] = parcelles_stats_filled.area
# ndvi_filled_mean = pd.DataFrame(parcelles_stats_filled.filter(regex='mean_*', axis=1), index=parcelles_stats_filled.index)
# ndvi_filled_mean.columns = pd.DatetimeIndex(DTIMES)


# Annuel
parc_2018 = ndvi_mean[ndvi_mean.columns[:25]]
parc_2018_old = parc_2018[parc_2018.index.isin(old_idx)]
parc_2018_non_old = parc_2018[parc_2018.index.isin(other_idx)]

parc_2019 = ndvi_mean[ndvi_mean.columns[25:]]
parc_2019_old = parc_2019[parc_2019.index.isin(old_idx)]
parc_2019_non_old = parc_2019[parc_2019.index.isin(other_idx)]

parc_2020 = ndvi_mean[ndvi_mean.columns[60:]]
parc_2020_old = parc_2020[parc_2020.index.isin(old_idx)]
parc_2020_non_old = parc_2020[parc_2020.index.isin(other_idx)]


# Stats sur les parcelles MTP contrôlées sur 2019
filter_idx = parcelles_mtp[(parcelles_mtp.codeident.isin(controle_mtp.codeident)) & (parcelles_mtp.aire >= 1000)].index
parc_filter = ndvi_mean[ndvi_mean.index.isin(filter_idx)]
parc_2019_filter = ndvi_mean[(ndvi_mean.index.isin(parc_2019.index)) & (ndvi_mean.index.isin(parc_filter.index))]

parc_st = parcelles_orig.join(ndvi_mean.mean(axis=1).rename('mean_ndvi_mean')).join(ndvi_mean.median(axis=1).rename('mean_ndvi_median')).join(ndvi_mean.min(axis=1).rename('mean_ndvi_min')).join(ndvi_mean.max(axis=1).rename('mean_ndvi_max')).join(ndvi_mean.std(axis=1).rename('mean_ndvi_std')).join(ndvi_std.mean(axis=1).rename('std_ndvi_mean')).join(ndvi_std.min(axis=1).rename('std_ndvi_min')).join(ndvi_std.max(axis=1).rename('std_ndvi_max'))
parc_st_nona = parc_st[np.invert(pd.isna(parc_st.std_ndvi_mean))]

fig, ax = plt.subplots(figsize=(12,2))
plt.scatter(DTIMES_PLEIADES, np.zeros_like(DTIMES_PLEIADES), marker='o', label="Pléiades", color='red')
plt.scatter(DTIMES[20:], np.zeros_like(DTIMES[20:]), marker='.', label='Sentinel 2')
ax.legend()
#fig.savefig('plots/date_plot_s2_pleiades.png', bbox_inches='tight')

# Parcelles
# parcelles_stats_pleiades = gpd.read_file('pleiades/stats/parcelles_ndvi_stats.shp')
# parcelles_stats_pleiades.set_index('gid', drop=False, inplace=True, verify_integrity=True)
# parcelles_stats_pleiades=parcelles_stats_pleiades.join(parcelles['classif'])

# parcelles_stats_pleiades['aire'] = parcelles_stats_pleiades.area
# parcelles_orig_pleiades = parcelles_stats_pleiades.filter(regex='[a-z]$')

# parcelles_mtp_pleiades = parcelles_orig_pleiades[parcelles_orig_pleiades['commune'] == '34172']
# parcelles_stats_mtp_pleiades = parcelles_stats_pleiades[parcelles_stats_pleiades['commune'] == '34172']

# parcelles_OLD_pleiades = parcelles_orig_pleiades[(parcelles_orig_pleiades.zone_old != 'aucune') & (parcelles_orig_pleiades.zone_old != 'sans obligation')]

# # NDVI PLEIADES
# ndvi_mean_pleiades = pd.DataFrame(parcelles_stats_pleiades.filter(regex='mean_*', axis=1), index=parcelles_stats_pleiades.index)
# ndvi_mean_pleiades = pd.DataFrame(np.where(ndvi_mean_pleiades < -1, np.nan, ndvi_mean_pleiades), index=parcelles_stats_pleiades.index)
# ndvi_mean_pleiades.columns = pd.DatetimeIndex(DTIMES_PLEIADES)

# # NDVI PLEIADES
# ndvi_std_pleiades = pd.DataFrame(parcelles_stats_pleiades.filter(regex='std_*', axis=1), index=parcelles_stats_pleiades.index)
# ndvi_std_pleiades = pd.DataFrame(np.where((ndvi_std_pleiades == 0) | (ndvi_std_pleiades > 1), np.nan, ndvi_std_pleiades), index=parcelles_stats_pleiades.index)
# ndvi_std_pleiades.columns = pd.DatetimeIndex(DTIMES_PLEIADES)

# # DIFF NDVI PLEIADES
# parcelles_stats_diff_pleiades = gpd.read_file('pleiades/stats/parcelles_ndvi_diff_stats.shp')
# parcelles_stats_diff_pleiades.set_index('gid', drop=False, inplace=True, verify_integrity=True)
# parcelles_stats_diff_pleiades=parcelles_stats_diff_pleiades.join(parcelles['classif'])

# ndvi_diff_mean_pleiades = pd.DataFrame(parcelles_stats_diff_pleiades.filter(regex='mean_*', axis=1), index=parcelles_stats_pleiades.index)
# ndvi_diff_mean_pleiades = pd.DataFrame(np.where(ndvi_diff_mean_pleiades < -1, np.nan, ndvi_diff_mean_pleiades), index=parcelles_stats_pleiades.index)
# ndvi_diff_mean_pleiades.columns = pd.DatetimeIndex(DTIMES_PLEIADES)

# TUP 
# parcelles_tup_ndvi_stats = gpd.read_file('s2/stats/parcelles_tup_ndvi_stats.shp')
# parcelles_tup_ndvi_stats_pleiades = gpd.read_file('pleiades/stats/parcelles_tup_ndvi_stats.shp')

# Plots
fig, ax = plt.subplots(figsize=(12,2))
plt.scatter(controle_mtp.c1_date, np.zeros_like(controle_mtp.codeident),
            marker='.', label='Contrôle 1')
plt.scatter(controle_mtp.c2_date, np.zeros_like(controle_mtp.codeident)-.002,
             marker='.', label='Contrôle 2')
ax.legend()
ax.set_xlim(datetime.date(2018, 1, 1), datetime.date(2020, 4, 1))
# fig.savefig('plots/plot_dates_controle.png', bbox_inches='tight')

# fig, ax = plt.subplots()
# parcelles_OLD_full.boxplot('aire', by='zone_old', ax=ax)
# fig.savefig('plots/boxplot_areas_parcelles_OLD_full.png', bbox_inches='tight')
# ax.set_ylim(0,1e04)
# fig.savefig('plots/boxplot_areas_parcelles_OLD_full_zoom.png', bbox_inches='tight')

fig, ax = plt.subplots()
parcelles_OLD_mtp.boxplot('aire', by='zone_old', ax=ax)
# fig.savefig('plots/boxplot_areas_parcelles_OLD_mtp.png', bbox_inches='tight')
ax.set_ylim(0,1e04)
# fig.savefig('plots/boxplot_areas_parcelles_OLD_mtp_zoom.png', bbox_inches='tight')

fig, ax = plt.subplots(figsize=(10,7))
ax.set_title('Classification K-means (n=6)')
ax.set_xlabel('Taux de bâti')
ax.set_ylabel('Couverture arborée')
plt.scatter(parcelles.bati_pc, parcelles.arbo_pct, c=parcelles.classif, s=parcelles.aire/1e04, cmap='gist_rainbow_r')
ax.legend()
# ax.set_yscale('log')
# fig.savefig(f'plots/KM_cluster_6_batipc_arbopct_FULL.png', bbox_inches='tight')

parcelles_stats_mtp = ndvi_mean[ndvi_mean.index.isin(parcelles_stats[parcelles_stats['commune'] == '34172'].index)]

if not os.path.exists('s2/stats/parcelles_ndvi_mean_mean.gpkg'):
    parcelles[['codeident','classif', 'geometry']].join(pd.DataFrame(ndvi_mean, columns=DATES)).to_file('s2/stats/parcelles_ndvi_mean_mean.gpkg', driver='GPKG')

if not os.path.exists('s2/stats/parcelles_ndvi_diff_mean.gpkg'):
    parcelles[['codeident','classif', 'geometry']].join(pd.DataFrame(ndvi_diff_mean, columns=DATES)).to_file('s2/stats/parcelles_ndvi_diff_mean.gpkg', driver='GPKG')


# controle_mtp_tdist = controle_mtp.join(controle_mtp.apply(get_nb_im, args=(ndvi_mean, 's2_'), axis=1))

# controle_mtp_tdist = controle_mtp_tdist.join(controle_mtp.apply(time_distances, args=(DTIMES, 's2_'), axis=1))
# controle_mtp_tdist = controle_mtp_tdist.join(controle_mtp_tdist.apply(time_distances, args=(DTIMES_PLEIADES, 'pl_'), axis=1))          

# if not os.path.exists('s2/stats/controle_mtp_time_distances.gpkg'):
#     controle_mtp_tdist.to_file('s2/stats/controle_mtp_time_distances.gpkg', driver='GPKG')

# ctrl = controle_mtp_tdist[(controle_mtp_tdist.index.isin(controle_mtp_2019.index)) & (controle_mtp_tdist.area >= 1000)]

# ctrl.to_file('s2/stats/controle_mtp_time_distances_2019.gpkg', driver='GPKG')

# ctrl = ctrl.join(ctrl.apply(ndvi_t_win_stat, args=(parcelles_orig, ndvi_diff_mean, 30, 'sum', 'diffmean_'), axis=1))
# ctrl = ctrl.join(ctrl.apply(ndvi_t_win_stat, args=(parcelles_orig, ndvi_diff_mean, 15, 'sum', 'diffmean_'), axis=1))
# ctrl = ctrl.join(ctrl.apply(ndvi_t_win_stat, args=(parcelles_orig, ndvi_diff_mean, 30, 'mean', 'diffmean_'), axis=1))
# ctrl = ctrl.join(ctrl.apply(ndvi_t_win_stat, args=(parcelles_orig, ndvi_diff_mean, 15, 'mean', 'diffmean_'), axis=1))

# ctrl = ctrl.join(ctrl.apply(ndvi_t_win_stat, args=(parcelles_orig, ndvi_diff_mean, 60, 'sum', 'diffmean_'), axis=1))
# ctrl = ctrl.join(ctrl.apply(ndvi_t_win_stat, args=(parcelles_orig, ndvi_diff_mean, 45, 'sum', 'diffmean_'), axis=1))
# ctrl = ctrl.join(ctrl.apply(ndvi_t_win_stat, args=(parcelles_orig, ndvi_diff_mean, 60, 'mean', 'diffmean_'), axis=1))
# ctrl = ctrl.join(ctrl.apply(ndvi_t_win_stat, args=(parcelles_orig, ndvi_diff_mean, 45, 'mean', 'diffmean_'), axis=1))

# ctrl = ctrl.join(ctrl.apply(ndvi_t_win_stat, args=(parcelles_orig, ndvi_mean_diff, 30, 'sum', 'meandiff_'), axis=1))
# ctrl = ctrl.join(ctrl.apply(ndvi_t_win_stat, args=(parcelles_orig, ndvi_mean_diff, 15, 'sum', 'meandiff_'), axis=1))
# ctrl = ctrl.join(ctrl.apply(ndvi_t_win_stat, args=(parcelles_orig, ndvi_mean_diff, 30, 'mean', 'meandiff_'), axis=1))
# ctrl = ctrl.join(ctrl.apply(ndvi_t_win_stat, args=(parcelles_orig, ndvi_mean_diff, 15, 'mean', 'meandiff_'), axis=1))

# ctrl.to_file('s2/stats/controle_mtp_mean_diffs.gpkg', driver='GPKG')