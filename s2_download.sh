#/bin/bash
WD=/media/nas/projects/POC_OLD/traitements/s2
cd $WD

mkdir -p zip_data/touch

for cc in 30 60; do
    cd $WD/zip_data/touch
    mkdir -p ../cc$cc

    python2 ../../theia_download.py -a ../../config_theia.cfg -c SENTINEL2 --level LEVEL2A -t T31TEJ -d 2020-01-01 -f 2020-09-30 -m $cc
    for zip in *.zip; do
        if [ -s "$zip" ]; then
            mv $zip ../cc$cc
            touch $zip
        fi
    done

    cd ../.. ; mkdir -p data2; cd data2

    for zip in ../zip_data/cc$cc/*.zip; do
        unzip $zip "*_FRE_B2.tif" "*_FRE_B3.tif" "*_FRE_B4.tif" "*_FRE_B8.tif" "*_CLM_R1.tif" "*_MG2_R1.tif" "*_QKL_ALL.jpg" "*_MTD_ALL.xml"
    done
done
