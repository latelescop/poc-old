#!/bin/bash
shopt -s globstar

export OTB_MAX_RAM_HINT=4096
#export GDAL_MAX_DATASET_POOL_SIZE=500

MASK_EXPR="(im1b1==0) && (im1b2==0) && (im1b3==0) && (im1b4==0) ? -9999 : im2"

mkdir -p /tmp/otb
cd /media/nas/projects/POC_OLD/traitements/pleiades

if [ -d "data" ]; then
    for a in `ls data/ORTHO_IGN_3M/*.vrt`; do
        basename=`basename $a`
        otbcli_BandMathX -il $a -exp "ndvi(im1b1,im1b4)" -out ORTHO_NDVI/${basename%.vrt}_NDVI.tif float
    done
    for a in `ls data/ORTHO_IGN/*.vrt`; do
        basename=`basename $a`
        otbcli_BandMathX -il $a -exp "ndvi(im1b1,im1b4)" -out ORTHO_NDVI/${basename%.vrt}_NDVI.tif float
    done

    for a in `ls data/ORTHO_NDVI`; do
        b_name=`basename $a`
        infile="data/ORTHO_IGN_3M/${b_name%_NDVI.tif}.vrt"
        if [ ! -e "$infile" ]; then
            infile="data/ORTHO_IGN/${b_name%_NDVI.tif}.vrt"
        fi
        otbcli_BandMathX -il "$infile" data/ORTHO_NDVI/$a -exp "$MASK_EXPR" -out "masked/${b_name%_NDVI.tif}.tif" float
    done

    if [ -f "masked/20190101.tif" ]; then
        gdalwarp -srcnodata -9999 -s_srs EPSG:2154 -r cubicspline masked/ORT_2019010138962072_LA93.tif masked/ORT_2019010138986890_LA93.tif masked/20190101.tif
    fi

    if [ ! -f "masked/20190429.tif"]; then
        gdalwarp -srcnodata -9999 -s_srs EPSG:2154 -r cubicspline masked/ORT_2019042938475212_LA93.tif masked/ORT_2019042938487819_LA93.tif masked/20190429.tif
    fi

    for d in masked/*.tif; do
        bn=`basename $d`
        gdalbuildvrt -srcnodata -9999 -vrtnodata -9999 -a_srs EPSG:2154 "${bn:4:8}.vrt" "$d"
    done

    if [ ! -f "20200311.vrt" ]; then
        otbcli_BandMathX -il "/media/vidlb/WD4To/LaTeleScop/data/pleiades/l93/20200311/IMG_PHR1A_MS_202003111052098_ORT_c8b7f759-b358-48f3-c8f2-f86dda07ac1a-002_R1C1_TOC_PXS_L93.tif" -exp "$MASK_EXPR" \
        -out "masked/IMG_PHR1A_MS_202003111052098_ORT_c8b7f759-b358-48f3-c8f2-f86dda07ac1a-002_R1C1_TOC_PXS_L93.tif "
        gdalbuildvrt -srcnodata -9999 -a_srs EPSG:2154 "20200311.vrt" "masked/IMG_PHR1A_MS_202003111052098_ORT_c8b7f759-b358-48f3-c8f2-f86dda07ac1a-002_R1C1_TOC_PXS_L93.tif"
    fi

    gdalbuildvrt -separate -srcnodata -9999 -a_srs EPSG:2154 "ndvi_stack.vrt" *.vrt
fi

if [ ! -f "dates.txt" ]; then
    for a in 20*.vrt ; do echo ${a%.vrt} >> "dates.txt" ; done
fi

if [ ! -f "stats/parcelles_ndvi_stats.shp" ]; then
    otbcli_ZonalStatistics -in "ndvi_stack.vrt" -inzone vector -inzone.vector.in "../parcelles.shp" -out vector -out.vector.filename "stats/parcelles_ndvi_stats.shp"
fi

if [ ! -f "stats/parcelles_tup_ndvi_stats.shp" ]; then
    otbcli_ZonalStatistics -in "ndvi_stack.vrt" -inzone vector -inzone.vector.in "../data/inter_old_tup/inter_old_tup.shp" -out vector -out.vector.filename "stats/parcelles_tup_ndvi_stats.shp"
fi


last_img=
for img in 20*.vrt; do
    outimg=diff/"${img%.vrt}.tif"
    if [ ! -e $outimg ]; then
        if [ -z $last_img ]; then
            otbcli_BandMathX -il $img -exp "0" -out diff/
        else
            otbcli_Superimpose -inr $img -inm $last_img -fv -9999 -out /tmp/otb/superimpose.tif
            otbcli_BandMathX -il /tmp/otb/superimpose.tif $img -exp '(im1b1  != -9999) && (im2b1 != -9999) ? im2b1 - im1b1 : -9999' -out /tmp/otb/diff.tif float
        fi
        otbcli_ManageNoData -in /tmp/otb/diff.tif -out $outimg -mode changevalue -mode.changevalue.newv -9999
        rm -rf /tmp/otb/diff.tif /tmp/otb/superimpose.tif
    fi
    last_img=$img
done

gdalbuildvrt -separate -srcnodata -9999 -vrtnodata -9999 -a_srs EPSG:2154 "diff_stack.vrt" diff/*.tif

if [ ! -f "stats/parcelles_ndvi_diff_stats.shp" ]; then
    otbcli_ZonalStatistics -in "diff_stack.vrt" -inzone vector -inzone.vector.in "../parcelles.shp" -out vector -out.vector.filename "stats/parcelles_ndvi_diff_stats.shp"
fi
