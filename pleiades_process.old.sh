#!/bin/bash
shopt -s globstar

export OTB_MAX_RAM_HINT=4096


RAW_DIR="/media/vidlb/WD4To/LaTeleScop/data/pleiades/raw"
TOC_DIR="/media/vidlb/WD4To/LaTeleScop/data/pleiades/toc"
PXS_DIR="/media/vidlb/WD4To/LaTeleScop/data/pleiades/pxs"
L93_DIR="/media/vidlb/WD4To/LaTeleScop/data/pleiades/l93"


mkdir -p $TOC_DIR
cd $TOC_DIR
for d in `ls $RAW_DIR`; do
    mkdir -p "$d"
    for img in `ls $RAW_DIR/$d/**/*.JP2`; do
        basename=`basename $img`
        outimg="${basename%.JP2}_TOC.tif"
        outpath="$d/$outimg"
        if [ ! -e "$outpath" ]; then
            otbcli_OpticalCalibration -in "$img" -level toc -clamp "true" -milli "false" -out "/tmp/$outimg" float
            otbcli_BandMathX -il "/tmp/$outimg" -exp "im1 * 10000" -out "$outpath" uint16
            if [ -e $outpath ]; then
                rm -f "/tmp/$outimg"
            fi
        fi
    done
done


mkdir -p $PXS_DIR
cd "$PXS_DIR"
for d in `ls $TOC_DIR`; do
    mkdir -p $d
    for img in `ls $TOC_DIR/$d/IMG_*_MS_*_TOC.tif`; do
        basename=`basename $img`
        date_str=${basename:13:15}
        outfile="$d/${basename%.tif}_PXS.tif"
        if [ ! -e "$outfile" ]; then
            pan=("$TOC_DIR/$d/IMG_"*"_P_$date_str"*".tif")
            otbcli_BundleToPerfectSensor -inp "$pan" -inxs $img -out "$outfile" uint16
        fi
    done
done


cd "$UTM_DIR"
for d in `ls $PXS_DIR`; do
    mkdir -p $d
    mkdir -p $PXS_COPY_DIR/$d
    for img in `ls $PXS_DIR/$d/*.tif`; do
        basename=`basename $img`
        outfile="$d/${basename%.tif}_UTM.tif"
        if [ ! -e $outfile ]; then
            otbcli_OrthoRectification -map utm -io.in "$img" -io.out $outfile uint16
            if [ -e $outfile ]; then
                cp -v $img $PXS_COPY_DIR/$d/$basename
                cp -v ${img%.tif}.geom $PXS_COPY_DIR/$d/${basename%.tif}.geom
                rm -f $img ${img%.tif}.geom
            fi
        fi 
    done
done


mkdir -p $L93_DIR
cd "$L93_DIR"
for d in `ls $PXS_DIR`; do
    mkdir -p $d
    for img in `ls $PXS_DIR/$d/*.tif`; do
        basename=`basename $img`
        outfile="$d/${basename%.tif}_L93.tif"
        if [ ! -e $outfile ]; then
            otbcli_OrthoRectification -map lambert93 -io.in "$img" -io.out $outfile uint16
        fi 
    done
done

for d in `ls`; do
    for img in `ls $L93_DIR/$d/*.tif`; do
        gdaladdo --config COMPRESS DEFLATE --config TILED YES --config BIGTIFF YES -r average -ro $img
    done
done