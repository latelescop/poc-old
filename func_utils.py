import datetime
import time

import numpy as np
import pandas as pd
import geopandas as gpd

from multiprocessing import Pool
from functools import partial


def to_datetimes(dates):
    "Convert str YYMMDD to datetime object"
    return [datetime.datetime.strptime(d, '%Y%m%d').date() for d in dates]


def _cloud_mask(time_serie, parcelles):
    "Set nan when polyon is within cloud"
    dtime = time_serie.name.to_pydatetime()
    date = dtime.strftime('%Y%m%d')
    clouds = gpd.read_file('s2/clouds_vector/' + date +'.shp')
    if len(clouds) > 0:
        cloudy_parc = parcelles[parcelles.within(clouds.unary_union)]
        time_serie[time_serie.index.isin(cloudy_parc.index)] = np.nan
    return time_serie


def _mp_cloud_masks(args, splitdf: gpd.GeoDataFrame):
    return splitdf.apply(_cloud_mask, args=args, raw=False)


def _parallelize(func, gdf, n_cores, chunks):
    """Split data, create a pool of workers, compute results in parallel and concat results"""
    data_split = np.array_split(gdf, chunks, axis=1)
    pool = Pool(n_cores)  
    data = pd.concat(pool.map(func, data_split), axis=1)
    pool.close()
    pool.join()

    return data


def clean_serie(stats, parcelles, n_cores=8, chunks=None):
    """Set any polygon within cloud to nan"""
    tmp_res = None
    if n_cores == 1:
        tmp_res = stats.apply(_cloud_mask, args= (parcelles,))
    elif n_cores > 1:
        if not chunks:
            chunks = len(stats.columns)

        _func = partial(_mp_cloud_masks, (parcelles,))
        tmp_res = _parallelize(_func, stats, n_cores, chunks)

    return tmp_res


def reject_outliers(data, m=2):
    f = abs(data - np.mean(data)) < m * np.std(data)
    return data.where(f, np.nan)


def now():
    return str(time.time()).split('.')[0]


def nearest_date(dt, dates):
    diff = pd.Series([(d - dt.date()).days for d in dates])
    idxmin = diff.abs().idxmin()
    value = diff[idxmin]
    return idxmin, value


def _ddist(d, dtimes):
    if pd.isnull(d):
        return None, None

    d_idx, value = nearest_date(d, dtimes)
    if value > 0:
        d_idx=d_idx-1
    dd1, d_before = None, None
    if 0 <= d_idx < len(dtimes):
        d_before = dtimes[d_idx]
        dd1 = (d_before - d.date()).days

    return str(d_before)[:10], dd1


def time_distances(serie, dtimes, suf='', data=None):
    d1 = serie.c1_date
    d2 = serie.c2_date
    res1 = _ddist(d1, dtimes)
    res2 = _ddist(d2, dtimes)

    cols=['d_avc1', 'j_avc1', 'd_avc2', 'j_avc2', ]
    cols = [ suf + c for c in cols ]
    res = [*res1, *res2]
    return pd.Series(res, index=cols)


def clean_values(values: pd.Series,
                 reject: int = 3,
                 interpolate: bool = True,
                 rolling_win: int = 2):

    if reject:
        values = reject_outliers(values, m=reject)
    if interpolate:
        values.interpolate('time', inplace=True)
    if rolling_win:
        values = values.rolling(rolling_win).mean()

    return values


def time_col_filter(df: pd.DataFrame, begin, end=None, delta=None):
    if pd.isnull(begin):
        return None
    try:
        date = df.columns[df.columns == begin][0]
    except IndexError:
        if isinstance(begin, pd.Timestamp):
            begin = str(begin)[:10]
        date = datetime.datetime.strptime(begin, '%Y-%m-%d')
    if delta and not end:
        end = date + datetime.timedelta(days=delta)
        if end < date:
            begin, end = end, begin
    elif (not end and not delta) or end == date:
        return df[df.columns[df.columns == begin]]

    return df[df.columns[(df.columns >= begin) & (df.columns <= end)]]


def get_nb_im(control, data, prefix='', nd=30):
    c1d, c2d = control.c1_date, control.c2_date
    nb_im_c1, nb_im_c2 = None, None
    if not pd.isnull(c1d):
        min_d = c1d - datetime.timedelta(days=nd)
        cols = data.columns[ (data.columns >= min_d) & (data.columns <= c1d)]
        nb_im_c1 = len(cols)
    if not pd.isnull(c2d):
        min_d = c2d - datetime.timedelta(days=nd)
        cols = data.columns[ (data.columns >= min_d) & (data.columns <= c2d)]
        nb_im_c2 = len(cols)
        
    col_index = [f'{prefix}nb_im_{nd}j_av_c1', f'{prefix}nb_im_{nd}j_av_c2']
    return pd.Series((nb_im_c1, nb_im_c2), index=col_index)

    
def filter_image_dist(controle_mtp, j_av, _and=False):
    filter_c1 = (controle_mtp.s2_j_avc1 >= -j_av)
    filter_c2 = (controle_mtp.s2_j_avc2 >= -j_av)
    if _and:
        return controle_mtp[(filter_c1 & filter_c2) ]
    return controle_mtp[(filter_c1 | filter_c2) ]


def ndvi_t_win_stat(values, parcelles, diff, days=30, typ='sum', prefix=''):
    
    cid = values.codeident
    data = diff[diff.index.isin(parcelles[parcelles.codeident == cid].index)]
    res = time_col_filter(data, values.c1_date, delta=-days)
    res1 = np.nan
    if res is not None:
        try:
            if typ == 'sum':
                res1 = np.nansum(res)
            elif typ == 'mean':
                res1 = np.nanmean(res)
        except IndexError:
            pass


    res = time_col_filter(data, values.c2_date, delta=-days)
    res2 = np.nan
    if res is not None:
        try:
            if typ == 'sum':
                res2 = np.nansum(res)
            elif typ == 'mean':
                res2 = np.nanmean(res)
        except IndexError:
            pass

    return pd.Series([res1, res2], index=[f'{prefix}c1_{typ}_-{days}j', f'{prefix}c2_{typ}_-{days}j']) 
